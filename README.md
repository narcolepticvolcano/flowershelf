<h1>Flowershelf</h1>

Work in progress ;)

<h2>Getting started</h2>

The optimal HTML structure is:
	
	<div id="yourGalleryID" class="flowershelf">
		<div class="yourRow">
			<div class="yourElement">
				<img class="yourImage" src="#">
				<div class="whatYouWant"></div>
			</div>
			[...]
		</div>
		[...]
	</div>


Initialize it with:

    <script type="text/javascript">
		var myGallery = new flowershelf(document.getElementById("yourGalleryID"),{options});
    </script>


<h2>Options</h2>
* __gutter__: space between elements
* __callbackFunction__: the callback
* __rowHeight__: if not null, the rows splits in another ones trying to achieve the row height
* __fullRows__: works only if rowHeight is set, default true, if false rows can end before the other (better if there's only one html row)

<h2 id="license">License (MIT)</h2>

Copyright (c) 2016-2017 Sara Potyscki, [thepot.site](http://www.thepot.site)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
